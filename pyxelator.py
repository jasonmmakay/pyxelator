from argparse import ArgumentParser
from itertools import product
from time import time_ns

from PIL import Image


class ProgressReporter:
    """Display the completion status of a long running task."""

    def __init__(self, total):
        """Initialize the status with the total number of sub-tasks."""
        self.length = 0
        self.count = 0
        self.total = total
        self.start = time_ns()
        self.previous = self.start

    def show(self, message):
        """Erase the previously printed status message and print a new one."""
        print(
            # backspace over all previous message characters
            '\b' * self.length,
            # print spaces to clear the display
            ' ' * self.length,
            # backspace over all spaces
            '\b' * self.length,
            # and print the message
            message,
            # do not use a spearator so that extra characters aren't added to
            # the output stream that we would then need to account for
            sep='',
            # do not output an end-of-line character so that we can 'delete'
            # characters from the stream
            end='',
            # flush so to ensure that the status is displayed
            flush=True)

        # update the buffer with the message
        self.length = len(message)

    def update(self):
        """Signal completion of a sub-task and display status every second."""
        now = time_ns()
        self.count += 1

        # if more than a second has passed, display some status
        if (now - self.previous) >= 1e9:
            # update our timer
            self.previous = now

            # figure the total number of seconds we've been running
            duration = (now - self.start) * 1e-9

            # show the current percent complete and total time taken
            self.show(
                '{:.2f}% in {:.0f} seconds'
                .format(100 * self.count / self.total, duration))

    def done(self):
        """Display a done message and break the output stream line."""
        # print the total time taken; include a new-line character to prevent
        # this status message from being overwriten
        duration = (time_ns() - self.start) * 1e-9
        self.show('done in {:.0f} seconds\n'.format(duration))

        # since we broke the line, reset the number of characters last printed
        self.length = 0


def parse_pixel_boxes():
    """
    Create prototypes of pixelation box images from text.

    Each prototype box is a 3x3 tuple of tuples of zeros and ones, indicating
    which color to use for each pixel in the box. The boxes are returned in a
    tuple.
    """
    # TODO: read this from a text file
    box_text = '''
        010
        121
        010
        ---
        101
        000
        101
        ---
        010
        101
        010
        ---
        000
        010
        000
        ---
        000
        000
        000
        '''

    boxes = []
    # each box is delimited by three dashes on a single line
    for box in box_text.split('---\n'):
        boxes.append([])
        # each row of the box is on its own line; strip leading and trailing
        # whitespace to avoid empty strings being produced
        for row in box.strip().split('\n'):
            # parse each zero or one in the row
            row = tuple(int(cell) for cell in row.strip())
            boxes[-1].append(row)

        # convert the row to a tuple and put it into the current box
        boxes[-1] = tuple(boxes[-1])

    # convert the result to a tuple (tuple of tuples of tuples of 0/1)
    return tuple(boxes)


def create_box_palette(palette, pallete_map):
    """
    Create a palette of small images and their average colors from a palette.

    One box is created for every possible combination of different colors from
    the palette and for each box returned from parse_pixel_boxes, for a total
    of len(palette) * (len(palette)-1) * len(parse_pixel_boxes()).

    TODO: This method currently assumes all pixel boxes are 3x3 and have only
    one or two colors in them.
    """
    box_palette = []

    boxes = set()
    for box in parse_pixel_boxes():
        # TODO: we assume 3x3 boxes, but they could be other dimensions
        assert len(box) == 3
        assert all(len(row) == 3 for row in box)

        # make sure that the cells in the box are numbered from 0 to n-1,
        # where n is the number of unique values in the box
        n = len({*flatten(box)})
        assert max(cell for cell in flatten(box)) + 1 == n

        for p in product(palette, repeat=n):
            # since we're using a set here, we don't need to worry about
            # ending up with duplicates
            boxes.add(tuple(p[c] for c in flatten(box)))

    for box in boxes:
        # construct an image with the pixels colored with the two colors from
        # the palette as specified by the box prototype and calculate the
        # average color of the pixels in it
        r = g = b = 0
        box_img = Image.new('RGB', (3, 3))

        for xy, p in zip(iter_pixel_coords(box_img), box):

            r += p[0]
            g += p[1]
            b += p[2]

            box_img.putpixel(xy, pallete_map.get(p, p))

        # finish calculating the average color
        r = round(r / 9)
        g = round(g / 9)
        b = round(b / 9)
        n = len({*box})

        # add the image and average color to the result palette
        box_palette.append((box_img, (r, g, b)))

    return tuple(box_palette)


def vector_distance_squared(a, b):
    """Calculate the squared distance between two vectors."""
    return sum((a0 - b0) ** 2 for a0, b0 in zip(a, b))


def argmin(x):
    """Find the index of the minimum value in an iterable."""
    # either a nop on an iterator or get an iterator for a container
    x = iter(x)

    # consume the first element and set it as the current minimum
    min_x = next(x)
    min_idx = 0

    for idx, x0 in enumerate(x):
        # we consumed the first element before the loop, we're really at idx+1
        idx += 1

        # if the element is a new minimum, update our minimum
        if x0 < min_x:
            min_x = x0
            min_idx = idx

    return min_idx


def nwise(x, n):
    """Convert a flat iterable into length n tuples."""
    it = iter(x)
    it = [it] * n
    return tuple(zip(*it))


def flatten(x):
    """Iterate over the elements of a double nested container."""
    for x0 in x:
        for x1 in x0:
            yield x1


def iter_pixel_coords(img):
    """Create an iterator oevr all of the pixel coordinates in an image."""
    return product(range(img.width), range(img.height))


def create_palette_image(palette):
    """Create an image to store a palette, required for quantization."""
    palette = [*flatten(palette)]
    flat_palette = palette[:3] * 256
    flat_palette[:len(palette)] = palette

    # this needs at least one pixel or the palette isn't saved
    img = Image.new('P', (1, 1))
    img.putpalette(flat_palette)
    return img


def pixelate_image(args):
    """Create a pixelated version of an image with a reduced color palette."""
    with Image.open(args.image_name) as img:
        assert img.mode == 'RGB'

        # if there is a palette specified, convert colors to it
        if args.palette:
            # convert the hex colors from the command line to integer triplets
            palette = [
                (int(rgb[0:2], 16), int(rgb[2:4], 16), int(rgb[4:6], 16))
                for rgb in args.palette]

            vq_img = img.quantize(palette=create_palette_image(palette))

        else:
            # calculate the vector quantized version of the image
            # NOTE: quantizing with just a number of colors creates the palette
            # with the used colors are packed into the front of the palette
            vq_img = img.quantize(args.num_colors)
            # vq_img.save('{0.image_name}.vq.{0.num_colors}.png'.format(args))

            # extract the color palette from the vq image and then create a
            # palette of small 3x3 boxes from those colors that will be pasted
            # into the result image to acheive 'pixelation'
            palette = nwise(vq_img.getpalette(), 3)[:args.num_colors]

        box_palette = create_box_palette(palette, {})

        # crop the image so that it is divisible by 3 - don't just resize to
        # prevent distortion (even though it would be minimal)
        box = [0, 0, img.width, img.height]
        if img.width % 3 >= 1:
            box[0] += 1
        if img.width % 3 >= 2:
            box[2] -= 1
        if img.height % 3 >= 1:
            box[1] += 1
        if img.height % 3 >= 2:
            box[3] -= 1

        # crop it
        # NOTE: the name of this variable is what it is because we will use the
        # same image to draw the pixelated result into
        pixel_img = img.crop(box)

        # resize the image to average surrounding pixels. we will sample from
        # this one to compare against average colors from the box palette when
        # selecting which of those boxes to paste into the result image
        # resize_img = pixel_img.resize((img.width // 3, img.height // 3))
        # resize_img = resize_img.quantize(256, Image.MEDIANCUT).convert('RGB')
        resize_dims = (img.width // 3, img.height // 3)
        resize_img = vq_img.convert('RGB').resize(resize_dims)

        # this can take a while, depending on the source image size, number of
        # colors and number of boxes, so report some status so it doesn't look
        # like we've hung
        progress = ProgressReporter(resize_img.width * resize_img.height)

        # create an iterator over all of the pixel coordinates in the resized
        # image and then load the pixel access object (NOTE: this seems to be
        # a bit faster than using Image.getpixel(), but that's subjective at
        # best here, no definitive proof of it).
        xy_iter = iter_pixel_coords(resize_img)
        resize_img = resize_img.load()

        # cache the box selected for each color so that we don't have to do the
        # math of calculating distances between colors if we see it again
        cache = {}
        for xy in xy_iter:
            rgb = resize_img[xy[0], xy[1]]

            # check our cache for whether we've already selected a box for it
            idx = cache.get(rgb)
            if idx is None:
                # first time we've seen this color, figure out which box to use
                idx = argmin(
                    vector_distance_squared(rgb, box[-1])
                    for box in box_palette)
                cache[rgb] = idx

            # paste the small box image into the result
            pixel_img.paste(box_palette[idx][0], (xy[0] * 3, xy[1] * 3))

            # make some noise
            progress.update()

        # print the 'done' message and show the result
        progress.done()
        pixel_img.show()


def main():
    """Parse the command line and pixelate the specified image."""
    parser = ArgumentParser(
        description='''
        Reduce the color palette and pixelate an image, to reproduce the lo-fi
        aesthetic of early video games.
        ''')
    parser.add_argument(
        'image_name', metavar='IMG',
        help='The file path to the image to pixelate.')
    parser.add_argument(
        '--num-colors', metavar='NUM', type=int,
        help='The number of colors to use if no palette is given.')
    parser.add_argument(
        'palette', metavar='RRGGBB', type=str, nargs='*',
        help='Hex triplets of RGB colors to use.')

    args = parser.parse_args()
    pixelate_image(args)


if __name__ == '__main__':
    main()
