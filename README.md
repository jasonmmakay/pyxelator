pyxelator is an experiment to reproduce the lo-fi images of the early gaming era.


# 4 colors
<img src="doc/sunset.vq.4.png" width="32%">
<img src="doc/sunset.jpg" width="32%">
<img src="doc/sunset.pixel.4.png" width="32%">


# 8 colors
<img src="doc/sunset.vq.8.png" width="32%">
<img src="doc/sunset.jpg" width="32%">
<img src="doc/sunset.pixel.8.png" width="32%">


# 16 colors
<img src="doc/sunset.vq.16.png" width="32%">
<img src="doc/sunset.jpg" width="32%">
<img src="doc/sunset.pixel.16.png" width="32%">


# Purples
<!--Colors: `#000000` `#000080` `#800080` `#008000` `#ffdf00`-->


<img src="doc/sunset.jpg" width="49%">
<img src="doc/sunset.purplegold.png" width="49%">

<img src="doc/sunset.purplebrowngold.png" width="49%">
<img src="doc/sunset.purple.png" width="49%">
